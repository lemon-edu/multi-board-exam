package io.vizend.board.aggregate.board.domain.entity.vo;

public enum DisplayNameType {
    //
    FamilyNameGivenName,                    // 홍길동
    FamilyNameSpaceGivenName,               // 홍길동
    FamilyNameCommaGivenName,               // Hong, Gildong
    GivenNameSpaceFamilyName,               // Steve Jobs
    FamilyNameGivenNameSpaceTitle,          // 홍길동 과장
    Nickname                                // hongs
}
